# Projet Infra

## Sommaire

- [Projet Infra](#projet-infra)
  - [Sommaire](#sommaire)
  - [Intro](#intro)
  - [Sujet](#sujet)
  - [Dates](#dates)
  - [Notation](#notation)
    - [Oral](#oral)
    - [Ecrit](#ecrit)

## Intro

Le projet infra vise à vous faire mettre en pratique vos compétences en système, infra, réseau et sécurité dans le but de répondre à un besoin réel.

C'est un terrain propice pour vous permettre de concrétiser les compétences acquises jusqu'à maintenant, pour aborder quelque chose de nouveau.

Le projet doit se faire en groupe de 2 à 5 personnes et dure ~2 mois.

## Sujet

**Le choix du sujet est libre**, vous devez vous-mêmes apporter votre sujet.

L'idée est la suivante :

- partez d'un vrai besoin, de quelque chose dont vous auriez besoin par exemple, dont vous seriez fiers
- ça doit mobiliser vos connaissances actuelles, en allant vers du savoir nouveau : vous devez pas juste répéter ce que vous savez déjà faire
- discussion autour de la faisabilité du projet
- réalisation du projet, en groupe

> Il sera nécessaire de bosser entre les séances allouées au soutien projet pour rendre quelque chose de correct.

## Dates

Le projet s'étendra sur 3 mois :

| Séance    | Date  |
|-----------|-------|
| Séance 1  | 07/02 |
| Séance 2  | 14/02 |
| Séance 3  | 21/02 |
| Séance 4  | 28/02 |
| Séance 5  | 07/03 |
| Séance 6  | 14/03 |
| Séance 7  | 28/03 |
| Séance 8  | 04/04 |
| Séance 9  | 11/04 |
| Séance 10 | 25/04 |
| Séance 11 | 02/05 |
| Séance 12 | 03/05 |

**La dernière séance sera consacrée aux oraux.**

## Notation

### Oral

L'oral prendra la forme d'une présentation de ~20 min. Elle comprendra au moins :

- présentation du **besoin**
- quel **type de solution** doit être utilisée ?
- quelle **solution spécifique** a été retenue (vos choix techniques ?
- **pourquoi** cette solution a été retenue ?
- **démonstration**

> La démonstration prouve que vous avez répondu au besoin, dans de bonnes conditions. Inutile de montrer tous les fichiers de conf etc, on veut VOIR des trucs qui fonctionnenent. On vous demandera de nous montrer les fichiers de conf si besoin.

Pendant la journée d'oral, tous les groupes noteront tous les autres groupes : la note finale sera une moyenne de toutes les notes.

Les 4 critères que l'on notera sont les suivants :

- challenge recherché /5
- pertinence de la problématique /5
- qualité de la présentation /5
- démonstration /5

### Ecrit

**L'écrit est un dépôt Git.** Il doit comporter au minimum :

- **une documentation d'installation complète** de votre solution
  - si on suit ces instructions, on peut remonter votre solution à l'identique
  - si la solution est déployée de façon automatisée, moins de doc sera nécessaire
- **un schéma** qui représente les différentes machines/entités impliquées dans la solution
- une section qui explique **comment utiliser la solution**, une fois qu'elle est en place, pour un utilisateur final
  - ça doit être bref
  - "rendez-vous sur `http://tel_adresse` pour accéder à l'application" par exemple
